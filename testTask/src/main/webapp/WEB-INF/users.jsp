<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="from" uri="http://www.springframework.org/tags/form" %>
<%@ page session="false" %>

<html>
<head>
    <title>User List</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>"/>
</head>
<body>

<a href="<c:url value="/index.jsp"/>">На главную страницу</a>
<br/>


<div class="actionsBlock">

<!-- Поиск ------------------------------------------------------------------------------------------------------------>
    <div class="searchBlock">
        <h2>Поиск пользователей</h2>

        <c:if test="${!isSearchResult && empty listUsers}">
            Искать не из кого
        </c:if>

        <c:if test="${!isSearchResult && !empty listUsers}">
            <form action="/search">
                <input type="text" name="userName">
                <input type="submit" value="Найти">
            </form>
        </c:if>

    </div>

<!-- Добавление и редактирование -------------------------------------------------------------------------------------->
    <div class="addEditBlock">
        <c:if test="${empty user.name}">
            <h2>Добавить пользователя</h2>
        </c:if>
        <c:if test="${!empty user.name}">
            <h2>Редактировать пользователя</h2>
        </c:if>

        <c:url var="addAction" value="/users/add"><c:param name="page" value="${param.page}"/></c:url>

        <form:form action="${addAction}" commandName="user">
            <table>
                <c:if test="${!empty user.name && user.age> 0 && user.age < 151}">
                    <tr>
                        <td>id</td>
                        <td>
                            <form:input path="id" readonly="true" size="8" disabled="true" cssClass="fm"/>
                            <form:hidden path="id"/>
                        </td>
                    </tr>
                </c:if>
                <tr>
                    <td>имя</td>
                    <td><form:input path="name" cssClass="fm"/></td>
                </tr>
                <tr class="error"><td colspan="2"><form:errors path="name" cssClass="error"/></td></tr>
                <tr>
                    <td>возраст</td>
                    <td><form:input path="age" cssClass="fm"/></td>
                </tr>
                <tr class="error"><td colspan="2"><form:errors path="age" cssClass="error"/></td></tr>
                <tr>
                    <td>администратор</td>
                    <td><form:checkbox path="admin" cssClass="fm"/></td>
                </tr>
                <tr>
                    <td><form:hidden path="createdDate"/></td>
                </tr>
                <tr>
                    <td colspan="2" align="right">
                        <input type="submit" value="<spring:message text="Сохранить"/>">
                    </td>
                </tr>
            </table>
        </form:form>
    </div>
</div>

<hr>

<!-- Список ----------------------------------------------------------------------------------------------------------->
<h2>Список пользователей</h2>

<c:if test="${isSearchResult && empty listUsers}">
    Нет пользователей с таким именем. Может ты опечатался или указал имя не полностью?
    <a href="<c:url value="/users"/>">Вернуться к полному списку</a>
</c:if>

<c:if test="${!isSearchResult && empty listUsers}">
    Пока никаких пользователей нет
</c:if>

<c:if test="${!empty listUsers}">

<!--          Пагинация ----------------------------------------------------------------------------------------------->
    <c:if test="${endpage > 1}">
        <div>
            <c:if test="${(param.page - 4) > 0}">
                <a href="<c:url value="/users"><c:param name="page" value="${param.page - 4}"/></c:url>"><<</a>
            </c:if>

            <c:forEach begin="${startpage}" end="${endpage}" var="p">

                <c:if test="${(param.page == p) || ((param.page == null || empty param.page) && (p == 1))}">
                    ${p}
                </c:if>

                <c:if test="${(param.page != p) &&(!((param.page == null || empty param.page) && (p == 1)))}">
                    <a href="<c:url value="/users" ><c:param name="page" value="${p}"/>${p}</c:url>"
                       class="paginator">${p}</a>
                </c:if>
            </c:forEach>

            <c:if test="${lastpage > endpage}">
                <a href="<c:url value="/users"><c:param name="page" value="${endpage + 1}"/></c:url>">>></a>
            </c:if>
        </div>
        <br/>
    </c:if>

<!--          Таблица пользователей ----------------------------------------------------------------------------------->

    <c:if test="${isSearchResult}">
        <a href="<c:url value="/users"/>">Вернуться к полному списку</a>
        <br/>
        <br/>
    </c:if>

    <table class="tg">
        <tr>
            <th width="80">id</th>
            <th width="200">имя</th>
            <th width="80">возраст</th>
            <th width="80">администратор</th>
            <th width="160">дата и время создания</th>
            <th width="80">Edit</th>
            <th width="80">Delete</th>
        </tr>
        <c:forEach var="user" items="${listUsers}">
            <tr>
                <td>${user.id}</td>
                <td>${user.name}</td>
                <td>${user.age}</td>
                <td>${user.admin}</td>
                <td><fmt:formatDate value="${user.createdDate}" pattern="dd.MM. yyyy HH:mm:ss"/></td>
                <td align="center">
                    <a href="<c:url value="/edit/${user.id}"><c:param name="page" value="${param.page}"/></c:url>">Edit</a>
                </td>
                <td align="center">
                    <a href="<c:url value="/remove/${user.id}"><c:param name="page" value="${param.page}"/></c:url>">Delete</a>
                </td>
            </tr>
        </c:forEach>
    </table>
</c:if>
<br/>

</body>
</html>
