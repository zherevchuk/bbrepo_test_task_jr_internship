package task.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import task.model.User;

import java.util.List;

@Repository
public class UserDaoImpl implements UserDao{
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void addUser(User user) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(user);
    }

    @Override
    public void updateUser(User user) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(user);
    }

    @Override
    public void removeUser(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        User user = session.load(User.class, id);
        if (user != null) {
            session.delete(user);
        }
    }

    @Override
    public User getUserById(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        return session.get(User.class, id);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<User> getUsersByName(String name) {
        Session session = this.sessionFactory.getCurrentSession();
        Query query = session.createQuery("from User u where u.name = :name order by u.createdDate desc");
        query.setParameter("name", name);
        return query.list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<User> getAllUsers(int page, int pageSize) {
        Session session = this.sessionFactory.getCurrentSession();
        Query query = session.createQuery("from User u order by u.createdDate desc");
        query.setFirstResult(page * pageSize - pageSize);
        query.setMaxResults(pageSize);
        return query.list();
    }

    @Override
    public long getLastPageNumber(int pageSize) {
        Session session = this.sessionFactory.getCurrentSession();
        Query query = session.createQuery("Select count (u.id) from User u");
        long countResults = (long) query.uniqueResult();
        return countResults % pageSize == 0 ? (countResults / pageSize) : (countResults / pageSize + 1);
    }
}
