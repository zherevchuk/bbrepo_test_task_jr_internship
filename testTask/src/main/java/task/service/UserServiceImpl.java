package task.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import task.dao.UserDao;
import task.model.User;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private UserDao userDao;

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    @Transactional
    public void addUser(User user) {
        this.userDao.addUser(user);
    }

    @Override
    @Transactional
    public void updateUser(User user) {
        this.userDao.updateUser(user);
    }

    @Override
    @Transactional
    public void removeUser(int id) {
        this.userDao.removeUser(id);
    }

    @Override
    @Transactional
    public User getUserById(int id) {
        return this.userDao.getUserById(id);
    }

    @Override
    @Transactional
    public List<User> getUsersByName(String name) {
        return this.userDao.getUsersByName(name);
    }

    @Override
    @Transactional
    public List<User> listUsers(int page, int pageSize) {
        return this.userDao.getAllUsers(page, pageSize);
    }

    @Override
    @Transactional
    public long getLastPageNumber(int pageSize) {
        return this.userDao.getLastPageNumber(pageSize);
    }
}
