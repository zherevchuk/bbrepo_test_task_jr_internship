package task.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import task.model.User;
import task.service.UserService;

import javax.validation.Valid;
import java.util.List;

@Controller
public class UserController {
    private UserService userService;
    private final int PAGE_SIZE = 4; // количество записей на одной странице
    private final int START_PAGE = 1; // для перехода на первую страницу списка, если ничего не найдено

    @Autowired(required = true)
    @Qualifier(value = "userService")
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "users", method = RequestMethod.GET)
    public String listUsers(@RequestParam(value = "page", required = false) Integer page, Model model) {
        model.addAttribute("user", new User());
        int currentPage = getPagination(page, model);
        model.addAttribute("listUsers", this.userService.listUsers(currentPage, PAGE_SIZE));
        return "users";
    }

    @RequestMapping(value = "users/add", method = RequestMethod.POST)
    public String addUser(@RequestParam(value = "page", required = false) Integer page,
                          Model model,
                          @ModelAttribute("user") @Valid User user,
                          BindingResult result) {

        if (result.hasErrors()) {
            int currentPage = getPagination(page, model);
            model.addAttribute("listUsers", this.userService.listUsers(currentPage, PAGE_SIZE));
            return "users";
        }

        if (user.getId() == 0) {
            this.userService.addUser(user);
            return "redirect:/users";
        } else {
            this.userService.updateUser(user);
            model.addAttribute("user", new User());
            int currentPage = getPagination(page, model);
            model.addAttribute("listUsers", this.userService.listUsers(currentPage, PAGE_SIZE));
            return "users";
        }
    }

    @RequestMapping(value = "/remove/{id}")
    public String removeUser(@PathVariable("id") int id) {
        this.userService.removeUser(id);
        return "redirect:/users";
    }

    @RequestMapping(value = "edit/{id}")
    public String editUser(@RequestParam(value = "page", required = false) Integer page,
                           @PathVariable("id") int id,
                           Model model) {

        model.addAttribute("user", this.userService.getUserById(id));
        int currentPage = getPagination(page, model);
        model.addAttribute("listUsers", this.userService.listUsers(currentPage, PAGE_SIZE));
        return "users";
    }

    @RequestMapping(value = "/search")
    public String findByName(@RequestParam("userName") String name, Model model) {
        model.addAttribute("user", new User());
        if (name == null || name.isEmpty()) {
            model.addAttribute("listUsers", this.userService.listUsers(START_PAGE, PAGE_SIZE));
            return "redirect:/users";
        } else {
            List<User> users = this.userService.getUsersByName(name);
            model.addAttribute("listUsers", users);
            model.addAttribute("isSearchResult", true);
        }
        return "users";
    }

    private int getPagination(Integer page, Model model) {
        int currentPage = 1;
        if (page != null && page > 0) {
            currentPage = page;
        }

        int startpage = currentPage - 3 > 0 ? currentPage - 3 : 1;
        long lastPage = this.userService.getLastPageNumber(PAGE_SIZE);
        long endpage = lastPage - currentPage > 3 ? currentPage + 3 : (lastPage == 0 ? 1 : lastPage);

        model.addAttribute("startpage", startpage);
        model.addAttribute("endpage", endpage);
        model.addAttribute("lastpage", lastPage);

        return currentPage;
    }
}

